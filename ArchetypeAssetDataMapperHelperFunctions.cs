﻿using Meep.Tech.Games.Unity.Extensions;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace Meep.Tech.Games.Unity.Data.Mapper {

  /// <summary>
  /// A map of file data to in game data.
  /// </summary>
  public static class ArchetypeAssetDataMapperHelperFunctions {

    /// <summary>
    /// Shortcut for getting a sprite from file
    /// </summary>
    /// <param name="spritePath"></param>
    /// <returns></returns>
    public static Sprite MapToSprite(string spritePath) {
      string fileName = spritePath + ".png";
      Sprite sprite = null;
      if (File.Exists(fileName)) {
        sprite = SpriteExtensions.LoadFromFilePath(fileName);
      }

      return sprite;
    }

    /// <summary>
    /// shortcut for getting a prefab model from an asset bundle
    /// </summary>
    /// <param name="assetPath"></param>
    /// <returns></returns>
    public static GameObject MapToPrefabModel(string assetPath) {
      return MapToPrefabModel(assetPath, out _);
    }

    /// <summary>
    /// shortcut for getting a prefab model from an asset bundle
    /// </summary>
    /// <param name="assetPath"></param>
    /// <returns></returns>
    public static GameObject MapToPrefabModel(string assetPath, out AssetBundle loadedModelBundle) {
      string fileName = assetPath;
      if (File.Exists(fileName)) {
        loadedModelBundle = AssetBundle.LoadFromFile(fileName);
        if (loadedModelBundle != null) {
          return loadedModelBundle.LoadAsset<GameObject>("model");
        }
      }

      loadedModelBundle = null;
      return null;
    }

    /// <summary>
    /// Get the models by mode.
    /// They're bundled in an asset with each model being named Mode#
    /// </summary>
    /// <returns></returns>
    public static GameObject[] GetModelsByMode(string assetPath) {
      string fileName = assetPath;
      int modeIndex = 0;
      if (File.Exists(fileName)) {
        List<GameObject> modelsByMode = new List<GameObject>();
        AssetBundle modelAssetBundle = AssetBundle.LoadFromFile(fileName);
        if (modelAssetBundle != null) {
          bool modelFound = true;
          while (modelFound) {
            GameObject model = modelAssetBundle.LoadAsset<GameObject>($"mode {modeIndex++}");
            if (model != null) {
              modelsByMode.Add(model);
            } else {
              modelFound = false;
            }
          }
        }

        return modelsByMode.ToArray();
      }

      return null;
    }
  }
}
